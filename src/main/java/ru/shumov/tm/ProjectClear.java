package ru.shumov.tm;

import java.util.Map;
import java.util.Scanner;

public class ProjectClear {

    private Map<String, String> projects;

    public ProjectClear(Map<String, String> projects) {
        this.projects = projects;
    }

    public void projectClear() {
        Scanner scrPrClear = new Scanner(System.in);
        System.out.println("Все проекты будут удалены, Вы уверны?");
        System.out.println("yes/no");
        String answer = scrPrClear.nextLine();

        if (answer.equals("yes")) {
            projects.clear();
            System.out.println("Done");

        }
    }
}
