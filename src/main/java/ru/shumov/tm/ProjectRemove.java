package ru.shumov.tm;

import java.util.Map;
import java.util.Scanner;

public class ProjectRemove {

    private Map<String, String> projects;

    public ProjectRemove(Map<String, String> projects) {
        this.projects = projects;
    }

    public void projectRemove() {
        Scanner scrProjectRe = new Scanner(System.in);
        System.out.println("Введите ID Проекта который вы хотите удалить:");
        String projectId = scrProjectRe.nextLine();
        if (projectId != null) {
            projects.remove(projectId);
        }
    }
}
