package ru.shumov.tm;

import java.util.ArrayList;
import java.util.Scanner;

public class TaskRemove {

    private ArrayList<String> tasks;

    public TaskRemove(ArrayList<String> tasks) {
        this.tasks = tasks;
    }

    public void taskRemove() {
        Scanner scrTaskRe = new Scanner(System.in);
        System.out.println("Введите имя проекта и задачи которую вы хотите удалить через пробел:");
        String taskName = scrTaskRe.nextLine();

        for (int i = 0; tasks.size() > i; i++) {
            if (tasks.get(i).equals(taskName)) {
                tasks.remove(tasks.get(i));
            }
        }
    }
}
