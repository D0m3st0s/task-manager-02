package ru.shumov.tm;

import java.util.Map;

public class ProjectList {

    private Map<String, String> projects;

    public ProjectList(Map<String, String> projects) {
        this.projects = projects;
    }

    public void projectList() {
        if (projects.isEmpty()) {
            System.out.println("Проектов нет");
        } else {
            System.out.println(projects);
        }
    }
}
