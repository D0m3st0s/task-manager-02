package ru.shumov.tm;

import java.util.ArrayList;
import java.util.Scanner;

public class TaskClear {

    private ArrayList<String> tasks;

    public TaskClear(ArrayList<String> tasks) {
        this.tasks = tasks;
    }

    public void taskClear() {
        Scanner scrTaskCl = new Scanner(System.in);
        System.out.println("Все задачи будут удалены, Вы уверны?");
        System.out.println("yes/no");
        String answer = scrTaskCl.nextLine();

        if (answer.equals("yes")) {
            for (int i = 0; tasks.size() > i; i++) {
                tasks.clear();
            }
        }
        System.out.println("Done");
    }
}
