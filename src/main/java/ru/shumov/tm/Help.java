package ru.shumov.tm;

public class Help {
    public void help() {

        System.out.println(Commands.HELP_HELP);
        System.out.println(Commands.HELP_PROJECT_CREATE);
        System.out.println(Commands.HELP_PROJECT_CLEAR);
        System.out.println(Commands.HELP_PROJECT_REMOVE);
        System.out.println(Commands.HELP_PROJECT_LIST);
        System.out.println(Commands.HELP_TASK_CREATE);
        System.out.println(Commands.HELP_TASK_CLEAR);
        System.out.println(Commands.HELP_TASK_REMOVE);
        System.out.println(Commands.HELP_TASK_LIST);
        System.out.println(Commands.HELP_FINISH);

    }
}
