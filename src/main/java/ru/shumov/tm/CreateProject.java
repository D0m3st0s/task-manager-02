package ru.shumov.tm;


import java.util.Map;
import java.util.UUID;
import java.util.Scanner;

public class CreateProject {

    private Map<String, String> projects;

    public CreateProject(Map<String, String> projects) {
        this.projects = projects;
    }

    public void createProject() {
        Scanner scrName = new Scanner(System.in);
        System.out.println("Введите имя проекта:");
        String name = scrName.nextLine();
        if (projects.containsValue(name)) {
            System.out.println("Такой проект уже существует");
        } else {
            String id = String.valueOf(UUID.randomUUID().toString());
            projects.put(id, name);
            System.out.println("Done");
        }
    }
}
