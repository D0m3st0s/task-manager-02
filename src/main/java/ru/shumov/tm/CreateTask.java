package ru.shumov.tm;

import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;


public class CreateTask {

    private Map<String, String> projects;
    private ArrayList<String> tasks;

    public CreateTask(ArrayList<String> tasks, Map<String, String> projects) {
        this.tasks = tasks;
        this.projects = projects;
    }

    public void createTask() {
        Scanner scrTasks = new Scanner(System.in);
        System.out.println("Чтобы добавить задание к проекту введите название проекта:");
        String projectName = scrTasks.nextLine();
        if (projects.containsValue(projectName)) {
            System.out.println("Введите назввание задачи:");
            String name = projectName + " " + scrTasks.nextLine();
            if (tasks.contains(name)) {
                System.out.println("Такая задача уже есть.");
            } else {
                tasks.add(name);
                System.out.println("Done");
            }
        } else {
            System.out.println("Такого проекта нет.");
        }

    }
}
