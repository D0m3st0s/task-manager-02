package ru.shumov.tm;

import java.util.ArrayList;
import java.util.Scanner;

public class TaskList {

    private ArrayList<String> tasks;

    public TaskList(ArrayList<String> tasks) {
        this.tasks = tasks;
    }

    public void taskList() {
        Scanner scrTaskList = new Scanner(System.in);
        if (tasks.isEmpty()) {
            System.out.println("Задач нет.");
        } else {
            System.out.println("Вывести все задачи?");
            System.out.println("yes/no");
            String answer = scrTaskList.nextLine();

            if (answer.equals("yes")) {
                for (int i = 0; tasks.size() > i; i++) {
                    System.out.println(tasks.get(i));
                }
            }
            if (answer.equals("no")) {
                System.out.println("Введите название проекта задачи которого нужно вывести:");
                String projectName = scrTaskList.nextLine();
                for (int i = 0; tasks.size() > i; i++) {
                    if (tasks.get(i).contains(projectName)) {
                        System.out.println(tasks.get(i));
                    }
                }
            }
        }
    }
}
