package ru.shumov.tm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;



public class Main {

    public static void main(String[] args) {
        Scanner scr = new Scanner(System.in);
        ArrayList<String> tasks = new ArrayList<>();
        Map<String, String> projects = new HashMap<>();

        CreateProject createProject = new CreateProject(projects);
        ProjectList projectList = new ProjectList(projects);
        ProjectClear projectClear = new ProjectClear(projects);
        ProjectRemove projectRemove = new ProjectRemove(projects);
        CreateTask createTask = new CreateTask(tasks, projects);
        TaskList taskList = new TaskList(tasks);
        TaskClear taskClear = new TaskClear(tasks);
        TaskRemove taskRemove = new TaskRemove(tasks);
        Help help = new Help();

        System.out.println("       Добро Пожаловать в Task Manager");
        System.out.println("...ID Проекта Можно Найти в Списке Проектов...");
        while (true) {

            String commandName = scr.nextLine();

            if (commandName.equals(Commands.HELP)) {
                help.help();
            }
            if (commandName.equals(Commands.PROJECT_CREATE)) {
                createProject.createProject();
            }
            if (commandName.equals(Commands.PROJECT_CLEAR)) {
                projectClear.projectClear();
            }
            if (commandName.equals(Commands.PROJECT_REMOVE)) {
                projectRemove.projectRemove();
            }
            if (commandName.equals(Commands.PROJECT_LIST)) {
                projectList.projectList();
            }
            if (commandName.equals(Commands.TASK_CREATE)) {
                createTask.createTask();
            }
            if (commandName.equals(Commands.TASK_CLEAR)) {
                taskClear.taskClear();
            }
            if (commandName.equals(Commands.TASK_REMOVE)) {
                taskRemove.taskRemove();
            }
            if (commandName.equals(Commands.TASK_LIST)) {
                taskList.taskList();
            }
            if (commandName.equals(Commands.FINISH)) {
                break;
            }
        }
    }


}
